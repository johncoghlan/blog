---
layout: page
title: README
permalink: /readme/
---

# A bit about me

My passion is building inclusive communities that help people love their work. This has taken me from managing large scale corporate volunteer projects at United Way of New York City to founding one of Brooklyn's first coworking spaces to becoming a community professional with a focus on technology. Currently, I manage Developer Evangelism for GitLab.

After spending nearly 15 years living, working, and attending school in New York City, I now live in the suburbs of New York City with my family. I love sports (Penn State/Giants/Yankees/Nets/Islanders) and coach youth soccer and basketball. I surf as often as I can...which isn't much. New York is not Hawaii. I'm a huge music fan - particularly Phish and the Grateful Dead - and enjoy listening to podcasts and reading books and blogs about my favorite bands. 

And if you need it, here is my [headshot](https://gitlab.com/johncoghlan/blog/-/blob/master/images/headshot.jpg).

# Values 

I love and live [GitLab's values](https://about.gitlab.com/handbook/values/) every day in both my professional and personal life (ex: assuming positive intent is great for all interactions). Outside of that, I'm inspired by the core values of Penn State football: 
- Positive Attitude 
- Work Ethic
- Compete
- Sacrifice 

I encourage you to [watch Coach James Franklin explain these values](https://youtu.be/cV3Au7glXzk?t=298) in detail. 

# How I like to work 

The fundamentals of my work style are:
1. Stick to a routine. I like to start and end every day at the same time. Each day begins and ends with 30 minutes of focused time to review and prioritize tasks, prep for upcoming meetings, and plan for the day ahead. 
1. Batch tasks. I am a terrible multi-tasker so I try to do related tasks at the same time.
1. Morning person. I'm most productive before lunch so I try to get as much creative work done as possible during those times. This is not always possible at GitLab because of our all-remote nature as mornings tend to be the best time to meet with colleagues in EMEA. To counteract this, I block off periods twice a week for deep work in the afternoons. This allows me to work uniterrupted during times when I find context switching more challenging.
1. GTD. I find success in the [Getting Things Done](https://en.wikipedia.org/wiki/Getting_Things_Done) approach to time management. I try to get quick tasks done in batches (i.e. responding to GitLab issues or Slack messages) and everything else goes on a to-do list (I use [Todoist](https://todoist.com/overview) and GitLab [Todos](https://docs.gitlab.com/ee/user/todos.html)). This helps me prioritize and not stress out over forgetting things. 
1. When leading teams, I want my team members to be able to check every box in [HBR's Pyramid of Employee Needs](https://hbr.org/resources/images/article_assets/2015/12/W151201_MANKINS_PYRAMIDOF-1024x720.png).


# Daily reminders

I have a set of reminders I read every day. They are:
1. Family first. (This is one of GitLab's sub-values)
1. Always be positive.
1. What are 3 things I am grateful for today?
1. What is the one thing you want to do today? (I write the answer to this down.)
1. Family first. (again)
1. Listen. Overcommunicate. No assumptions.
1. You can do this. Don't be shy. Step in, step up, and lead.
1. Influence and help everyone. Build and strengthen your sphere of influence and network every day.
1. Take feedback and grow from it. People are trying to help - not hurt. They are giving feedback because they care.
1. Don't be intimidated - you are the best person for this job and a huge asset to GitLab. 
1. SHIP SHIT.

I also have the core values for Penn State in my to do list and as my computer background as a reminder of those. 

# Other things to know 

- I'm loyal to a fault. 
- When I say I will do something, I deliver. 
- I believe autonomy, mastery, and purpose are the keys to motivation. 
