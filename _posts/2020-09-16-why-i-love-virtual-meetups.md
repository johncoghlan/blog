---
layout: post
title: "Why I love virtual meetups"
permalink: /:title/
---

Since March, GitLab's meetups (which I manage) have been entirely virtual. This shift to virtual events is not unique - most industries from education to healthcare to media have similarly shifted to virtual events and remote work in an effort to slow the spread of COVID-19. I suspect our virtual events experience has been shared by thousands, maybe millions, of others. So while our experience has been shared by many, we've still learned some important lessons from the dozens of virtual community events we have organized. These lessons will impact our community and others in the coming months and years. 

## What we know about virtual meetups

Here are some of the things we've learned:

### Interaction is key 

Many tech community meetup groups tend to measure success in the size of their groups and events. While there is certainly value in generating awareness for your community through large groups and well-attended events, much of the value for your meetup members and event attendees comes in the informal 1:1 or small group interactions they have at your events. With virtual events, you must be much more intentional in how you foster interactions as folks won't have a chance to strike up a conversation while grabbing a drink or finding a seat. 

Because of the challenges around facilitating informal interactions with virtual events, there can actually be more value in bringing together folks in small groups. Within the GitLab community, we encourage folks to allocate time for group discussions during their events. We typically use breakout groups at the end of our events to foster discussion. [Bethan Vincent](https://twitter.com/BethanVincent), a [GitLab Hero](https://about.gitlab.com/community/heroes), suggests ice breakers and introductions to help folks understand who else is in attendance and provide networking opportunities. She notes that may not work in groups with more than 20 attendees, so breakout groups again, would be an option to facilitate intros and icebreakers. 

You can hear more about what Bethan has to say about virtual meetups in this YouTube video from June 2020. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/lvWxMfiL_T8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It's also important to shift your mindset when thinking about the success of virtual events. In many cases, smaller groups are actually more productive and enjoyable to be a part of in a virtual setting than  a large group. So don't allow yourself to get caught up in the number of RSVPs. Remind yourself that everyone who is supposed to be there will be there. 

This quote from another GitLab Hero and organizer of the GitLab Meetup Hamburg, Philipp Westphalen, shows that a small event can have a big impact:
>"It went great, we had some deep discussions in a small group (we were 3 or 4 people). Turned out that these sessions are really great for helping other in the community with their interest."

You can see the context in this [GitLab issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/issues/996#note_352464182).

And just for fun, here is a tweet from Phil from one of the first GitLab virtual meetups we did in March.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">My first <a href="https://twitter.com/hashtag/remote?src=hash&amp;ref_src=twsrc%5Etfw">#remote</a> meetup by <a href="https://twitter.com/gitlab?ref_src=twsrc%5Etfw">@gitlab</a> with <a href="https://twitter.com/dnsmichi?ref_src=twsrc%5Etfw">@dnsmichi</a> <a href="https://twitter.com/john_cogs?ref_src=twsrc%5Etfw">@john_cogs</a> and many others 🥳 <a href="https://t.co/jC5qsvnKxH">pic.twitter.com/jC5qsvnKxH</a></p>&mdash; Philipp (@Koala_Phil) <a href="https://twitter.com/Koala_Phil/status/1242486989087051776?ref_src=twsrc%5Etfw">March 24, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Diversity and inclusion 

Diversity and inclusion is critical to the success of communities, organzations, and society.

Here are a few ways virtual events are more diverse and inclusive: 
- The nature of virtual events means they can be open to anyone with an internet connection.
- Geographic restrictions are no longer a limit.
- Given the plethora of virtual events happening each day, folks can likely find an event to join most days of the week no matter how busy their schedule.
- Folks dealing with physical issues that make attending in-person events difficult are now more likely to be able to participate in the events of their choosing.
- The ease of recording virtual events has increased the availability of recordings which are accessible at any time. These recordings can be captioned, annotated, and converted to text making them easier to comprehend for folks with different abilities and learning styles. 
- Many virtual events are free to attend opening them up to folks who could previously not afford them. 

### Virtual community events are here to stay

In addition to the diversity and inclusion benefits described above, groups are realizing many other advantages to virtual events. Two of the greatest benefits are the efficiency and scalability of these events. 

Virtual events allow you to reach a global audience, growing your community without any geographic restrictions. You can also pull from a global network of speakers which makes your events more diverse, inclusive, and interesting. It makes finding speakers a bit easier, too. Travel - for speakers and attendees - is completely eliminated. In cities like NY or London, this might save people an hour or two of their day. Venues and other costs are gone, too. 

There is still a steep learning curve as folks learn new platforms and adjust to virtual. But soon, the time spent looking for sponsors to provide a venue or buy pizza for your meetup will be spent on communication and content or maybe just spending less time planning meetups. 

## Looking into the future 

The future of virtual community events will be a hybrid model of both virtual and in-person events. Larger communities will emphasize virtual events at the global and local-level while folks in those groups self-organize into smaller groups that meet in-person on their own schedules to work together on side projects and coding exercises. You see some of [this happening now](https://everyonecancontribute.com/).

To foster this model, community leaders at the global level will need to organize events at the global level, encourage organizers to lead events at the local level, and foster the formation of these smaller groups. The group formation can be supported through breakout sessions during virtual events, events designed *specifically* for this cause, or via online forums and chat platforms. I suspect, new tools will emerge to help foster these connections, as well.  

If you'd like to follow along as we go through this journey at GitLab, make sure to join our [global virtual meetup group](https://www.meetup.com/gitlab-virtual-meetups/). If you have feedback or thoughts, feel free to tweet me: [@john_cogs](https://twitter.com/john_cogs). 

