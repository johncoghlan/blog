---
layout: post
title: "Some thoughts on measuring DevRel"
permalink: /:title/
---

Developer Relations, community, and marketing folks often grapple with understanding the impact of the content they create. 

Metrics like page views and unique page views tell one story. But it's hard to grok whether the eyeballs are the right eyeballs. 

More telling metrics - like trials or other conversion-oriented metrics - can be hard to attribute to a specific piece of content or method of delivery. 

Overall, DevRel and community teams should focus not on the piece by piece results of their content creation efforts but on the cumulative results of those efforts. This helps filter out the noise and makes attribution a bit easier. 

For blog posts that might look like this: 
- if we know that our marketing site generates X leads and the DevRel team is driving Y% of the traffic to the site, then we can estimate that X x Y% of the leads generated by the site can be attributed to the DevRel team. 

This can also work for events:
- if Pipe to Spend for events with a GitLab speaker at the event is X and the Pipe to Spend for events without a GitLab speaker is Y, then we can estimate speakers can boost ROI on events by (X / Y)%.

Would love to hear if you agree/disagree and how you measure DevRel in your orgs. Hit me up on [LinkedIn](https://www.linkedin.com/in/johnwcoghlan/) if you want to chat about it. 
