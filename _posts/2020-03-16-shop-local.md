---
layout: post
title: "Shop local (safely) during the pandemic"
permalink: /:title/
---

Just like you, I love my hometown. One reason we love our neighborhoods are the small businesses nearby. Owned and staffed by our neighbors, friends, and classmates, these businesses are uniquely ours. They sponsor our sports teams and support our local causes. They know your order and are the places we go to connect with each other. The reality of the current situation is that right now - these businesses are in trouble. 

We're all feeling the impact of coronavirus - and the concerns about our families' health, our jobs, and the future are *very* real.  I can't predict what will happen but I know one thing that will make you feel better: supporting your favorite small businesses during their time of need. 

Here's how to do it while practicing responsible social distancing which is critical to slowing the spread of the virus: 

### Take advantage of curbside pickup and delivery

Many businesses, especially restaurants and cafes, offer delivery and takeout. In some states, cities, and municipalities this is the only thing these businesses can do right now. Call your favorite local spots and place an order (if not, encourage them do so!). If you prefer to avoid handing over cash or your credit card, ask them if they take Venmo. 

### Buy a giftcard via Venmo 

For some businesses, such as boutiques or surf shops, you may not have an immediate need for their services. You can still support them by purchasing gift cards, ask if you can do this via Venmo so you don't need to go to the store, to provide them with cash flow now which can help them weather the slow down. 

### Book a virtual class or appointment

Facetime and Zoom are amazing communication tools. Take advantage of them. Call your therapist, yoga instructor, personal trainer, or guitar teacher and book virtual appointments.  The human connection and semblance of your normal routine will be benefit your mental health and provide your favorite service providers with much needed support. 

### Support on Patreon 

Tell your favorite business owners to get on Patreon. Patreon allows folks to contribute fixed amounts monthly in exchange for access, content, etc. The ways this can be leveraged are countless and unique to each business. I'm happy to brainstorm with small business owners who are interested in learning more on how they can connect with and provide value to their communities in exchange for contributions. 

### Call your local politician 

Despite the steps outlined above, the reality is that many businesses will require support from local, state, and federal government to overcome the impacts of coronavirus. Call and email your local officials and make sure they understand that support for small business during this crisis is a priority for your community. 

### Spread the word 

Share this post. Share how you're helping on social media and group chats. Encourage your favorite businesses to rethink how they are conducting their businesses. And if you have other ideas, please send them my way so I can update this post. 


