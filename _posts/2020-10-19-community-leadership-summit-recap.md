---
layout: post
title: "Community Leadership Summit 2020 Recap"
permalink: /:title/
---

This year is my first time participating in the Community Leadership Summit. Things are a bit different this year (which can be said about pretty much everything in 2020), most notablythe event is completely virtual and it's now co-located with All Things Open (rather than OSCON). What hasn't changed is that the event features a number of sessions intended to be conversations rather than presentations.

All Things Open's site has the [full list of sessions and moderators](https://2020.allthingsopen.org/events/community-leadership-summit-2020/) for Community Leadership 2020. I participated in / plan to participate in these sessions: 
- [Connecting Community/DevRel Teams to Other Teams lead by Mary Thengvall](https://2020.allthingsopen.org/sessions/connecting-community-devrel-teams-to-other-teams/)
- [Measuring Community Health by Samantha Logan](https://2020.allthingsopen.org/sessions/measuring-community-health/)
- [How to Build Amazing Incentives and Rewards (panel and group discussion) lead by Jono Bacon](https://2020.allthingsopen.org/sessions/how-to-build-amazing-incentives-and-rewards-panel-and-group-discussion/)

Due to soccer practice, I'll miss the last sessions and the event wrap up but I'm hoping to consume more of the sessions asychronously via notes and recordings.  

## Connecting Community/DevRel teams to other teams

This session was moderated by one of the leading voices in DevRel, [Mary Thengvall](https://twitter.com/mary_grace). If you're in the DevRel or community field, I highly recommend following Mary and subscribing to her newsletter, [DevRel Weekly](https://devrelweekly.com/).

This session focused mainly on how to avoid and eliminate the friction that sometimes exists between DevRel and other teams due to the multi-faceted nature of DevRel teams. The key takeaways include: 
- Work in _partnership_ with other teams rather than doing work _for_ them
- Team them how to fish
- Prioritize the work and show the _value_ of the work that you’re doing internally.

Throughout the discussion, the importance of effective communication between teams was a common thread. 

During the discussion, I shared some of the work we're doing at GitLab. Specifically, I referenced an [open merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/65127) that outlines the creation of our new DE budgeting process.The idea is to create budgets for our internal stakeholders to limit the number of requests for our team. The hope is that these limits will force our internal stakeholders to prioritize requests for our team so that 
- We’re handling the highest priority / highest impact requests
- We're allowing the folks closest to the request to ensure it is of sufficient priority to request support from our team
- We're have the ability and time to deliver great results for each requestor 
- We're capping the amount of work our team is doing to avoid burnout 

## Measuring community health 

Thanks to Samantha Venia Logan for moderating. This was a really great session with lots of participation in the Zoom call and chat. Some highlights: 

**What should communities measure?**
  - Communicate with your stakeholders and make sure you're tracking what matters to them.
  - Avoid vanity metrics.

**How can you measure what is coming from the community?**
  - At GitLab, we use labels to track community contributions but I recognize this doesn't work for all organizations. 
  - [Grimoire Labs](https://chaoss.github.io/grimoirelab/), [Cauldron](https://cauldron.io/), and [OpenHub](https://www.openhub.net/) were referenced as tools used for tracking community activity for open source projects. 

**How can you measure the impact of what you are tracking?**
  - Suggested that tracking the number of emoji-reactions on the issues becoming closed by community contributions. This was quickly captured by Georg Link as an [issue](https://github.com/chaoss/wg-evolution/pull/378) in the CHAOSS project. 
  - How to handle a high volume of requests:
    - Dawn Foster used the Kubernetes community as an example when suggesting communities create programs around the types of requests you're seeing in large volume. 
    - Andrea Middleton reminded us of the important of having a clear scope for what your community can and cannot support (ex: "we're not able to provide career counseling"). 


## How to build amazing incentives and rewards

This one was moderated by [Jono Bacon](https://twitter.com/jonobacon), who also happens to be the leader of the Community Leadership Summit. The talk covered a lot of familiar ground. Notably the issues with swag and instrinsic vs extrinsic motivation. 

Some concepts or ideas that were new to me included:
- submarine (vs stated) incentives, which are rewarded to folks as a surprise for participation. 
- providing rewards to folks who have their contributions rejected.  
- the risk of over-rewarding your community (a risk when using extrinsic motivaion)
- ensuring your incentives are inclusive

The inclusivity piece was particularly interesting. Ensuring that your incentives and rewards are inclusive is hugely important to making folks from diverse backgrounds and experience feel welcomed in your community. This starts with the types of swag you offer but extends to the events you plan for community members and other types of rewards. 

## Closing thoughts 

Today was great. This was my first time blocking off an entire day for a virtual conference, with the exception of GitLab Commit, and it really allowed me to dig in to the event and participate in a much more meaningful way than other virtual events. The format of this event certainly contributed to that, too. Each CLS session encouraged participation which resulted in interactive and interesting discussions. I think that participation was boosted even by because the audience being largely community folks who are comfortable participating in events and know that their participation is key to the event's success. 

Due to soccer practice, I'll miss the last sessions and the event wrap up but I'm hoping to consume more of the sessions asychronously via notes and recordings. Hopefully next year, we'll be able to participate in this great community event in-person. 

