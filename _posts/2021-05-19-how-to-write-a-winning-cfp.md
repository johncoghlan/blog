---
layout: post
title: "Video: How to write a winning CFP"
permalink: /:title/
---

I recently presented at a GitLab meetup on how to write a winning CFP for our upcoming [GitLab Commit](https://about.gitlab.com/events/commit/) conference. I figured I would share as some of the insights are helpful regardless of the conference. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/mFVuKjN1tlc?start=960" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Note: the video will start at the 16:00 minute mark as the first part of the recording is specific to our Commit event.*

You can find the slides from the presentation here: [GitLab Commit 2021 - CFP Worskhop](https://docs.google.com/presentation/d/16A1sPq9H5_8nsoAckUUcmgaFBaP1ex9Ci2dO8SNFNM0/edit?usp=sharing). 
