---
layout: post
title: The Evangelist's Reading List
permalink: /:title/
---

Tech evangelism or developer advocacy or whatever you choose to call it is booming. A quick search on LinkedIn for three of the most popular titles for what we do (technical evangelist, developer advocate, developer relations) returned more than 12,000 people and around 300 job openings. Because this profession is still new and evolving the education process remains largely self-driven.</p>
Luckily, we have the Evangelism Collective - a group of technical evangelists, developer advocates, and other community builders. The core of the community is a Slack channel that is a mix of equal parts camaraderie, mentorship, and shared suffering. It is a vibrant community and a great place to learn about what is happening in the field.
<p>I recently posted a request for book recomemendations. Not surprisingly, the community was quick to respond. Because it is a free community, at some point, the older parts of our history will be replaced by a recent message. So in order to preserve the knowledge of the group, I wanted to quickly jot down this community-sourced evangelism reading list (along with a few of my own picks).</p>
<p>If you are not a tech evangelist, you may still find some interesting recommendations in this list of titles. I was certainly surprised by the range of responses. Take a look:</p>
<ul>
<li>Freakonomics: A Rogue Economist Explores the Hidden Side of Everything by Steven Levitt, Stephen Dubner</li>
<li>Think Like a Freak: The Authors of Freakonomics Offer to Retrain Your Brain by Steven D. Levitt, Stephen J. Dubner</li>
<li>SuperFreakonomics: Global Cooling, Patriotic Prostitutes, and Why Suicide Bombers Should Buy Life Insurance by Steven D. Levitt, Stephen J. Dubner</li>
<li>The Challenger Sale: Taking Control of the Customer Conversation by Matthew Dixon, Brent Adamson</li>
<li>The Trusted Advisor by David H. Maister, Charles H. Green</li>
<li>TED Talks: The Official TED Guide to Public Speaking by Chris Anderson</li>
<li>The Power Presenter: Technique, Style, and Strategy from America's Top Speaking Coach by Jerry Weissman</li>
<li>Made to Stick: Why Some Ideas Take Hold and Others Come Unstuck by Chip Heath</li>
<li>Switch: How to Change Things When Change Is Hard by Chip Heath</li>
<li>The Cluetrain Manifesto: 10th Anniversary Edition by Rick Levine, Christopher Locke</li>
<li>Never Eat Alone: And Other Secrets to Success, One Relationship at a Time by Keith Ferrazzi, Tahl Raz</li>
<li>Resonate: Present Visual Stories that Transform Audiences by Nancy Duarte</li>
<li>Thinking in Systems: A Primer by Donella H. Meadows, Diana Wright</li>
<li>Principles: Life and Work by Ray Dalio</li>
<li>Powerful: Building a Culture of Freedom and Responsibility by Patty McCord</li>
<li>Give and Take: Why Helping Others Drives Our Success by Adam Grant</li>
<li>Deep Work: Rules for Focused Success in a Distracted World by Cal Newport</li>
<li>Essentialism: The Disciplined Pursuit of Less by Greg McKeown</li>
<li>Tools of Titans: The Tactics, Routines, and Habits of Billionaires, Icons, and World*Class Performers by Timothy Ferriss</li>
<li>Joel on Software: And on Diverse and Occasionally Related Matters That Will Prove of Interest to Software Developers, Designers, and Managers, and to Those Who, Whether by Good Fortune or Ill Luck, Work with Them in Some Capacity by Joel Spolsky</li>
</ul>
<p>(Note: I opted not to include links to an online bookseller in hopes that you'll support your local indie bookstore or library.)</p>
<p>Special thanks to all who chimed in with a response:</p>
<ul>
<li><a href="https://twitter.com/harrisja" rel="nofollow">Jason Harris</a></li>
<li><a href="https://twitter.com/mattstratton" rel="nofollow">Matt Stratton</a></li>
<li><a href="https://twitter.com/emckean" rel="nofollow">Erin McKean</a></li>
<li><a href="https://twitter.com/crayzeigh" rel="nofollow">Aaron Aldrich</a></li>
<li><a href="https://twitter.com/mary_grace" rel="nofollow">Mary Grace</a></li>
<li><a href="https://twitter.com/thomasj" rel="nofollow">James Thomas</a></li>
<li><a href="https://twitter.com/PreciselyAlyss" rel="nofollow">Alyss</a></li>
</ul>
